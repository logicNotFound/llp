# LifeLiftPro Backend

## Overview

This repository contains the backend code for the LifeLiftPro project, a comprehensive EHR/EMR SaaS for psychologists, psychiatrists, counselors, and coaches.

## Technologies Used

- Node.js
- Express
- MongoDB using MongoAtlas(Database)

## Installation

1. Clone the repository:

```
git clone https://gitlab.com/webig/lifeliftpro.git
```

2. Navigate into the project directory and install dependencies:

```
cd lifeliftpro
npm install
```

## Seed the Project

To run the project, use the following command:

```
npm run seed
```

## Running the Project

To run the project, use the following command:

```
npm start
```

## License

This project and its source code are proprietary and confidential, owned by Webig (A unit of THNM GROUP O.E.). Unauthorized copying of this repository, via any medium, is strictly prohibited. All rights reserved.
