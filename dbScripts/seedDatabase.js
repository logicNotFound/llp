const mongoose = require("mongoose");
const User = require("../src/models/User"); // Adjust the path as needed
const Role = require("../src/models/Role"); // Adjust the path as needed
const Permission = require("../src/models/Permission"); // Adjust the path as needed
const bcrypt = require("bcryptjs");

// VAT Number Generation Function
function generateVatNumber() {
  const letters =
    String.fromCharCode(65 + Math.floor(Math.random() * 26)) +
    String.fromCharCode(65 + Math.floor(Math.random() * 26));
  const numbers =
    Math.floor(Math.random() * (999999999 - 100000000 + 1)) + 100000000;
  return letters + numbers;
}

async function seedDatabase() {
  await mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  // Create Roles
  const roles = [
    "admin",
    "psychologist",
    "counselor",
    "coach",
    "psychiatrist",
    "secretary",
    "trial_user",
  ];
  const roleDocs = await Role.insertMany(roles.map((name) => ({ name })));

  // Create Permissions
  const permissions = [
    "create-branch",
    "update-branch",
    "delete-branch",
    "create-company",
    "update-company",
    "delete-company",
    "view_own_records",
    "edit_own_records",
    "create_records",
    "manage_own_appointments",
    "export_patient_data",
    "import_patient_data",
    "manage_all_appointments",
    "send_communications",
  ];
  const permissionDocs = await Permission.insertMany(
    permissions.map((name) => ({ name }))
  );

  // Associate permissions with roles
  // Here, for simplicity, we associate all permissions with all roles.
  // You can customize this as needed.
  for (const role of roleDocs) {
    role.permissions = permissionDocs.map((p) => p._id);
    await role.save();
  }

  // Create Users and associate roles
  const hashedPassword = await bcrypt.hash("123456", 12);
  const users = [
    {
      firstName: "Admin",
      lastName: "User",
      email: "admin@example.com",
      password: hashedPassword,
      vatNumber: generateVatNumber(),
      emailVerifiedAt: new Date(),
      roleId: roleDocs.find((r) => r.name === "admin")._id,
    },
    // Add more users here
  ];

  await User.insertMany(users);

  console.log("Database seeded successfully");
  mongoose.connection.close();
}

seedDatabase().catch(console.error);
