require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const helmet = require("helmet");
const rateLimit = require("express-rate-limit");
const authRoutes = require("./src/routes/auth");
const patientRoutes = require("./src/routes/patient");
const appointmentRouts = require("./src/routes/appointment");
const companyRoutes = require("./src/routes/company");

const app = express();

// Security middleware
app.use(helmet());
// Rate limiting
const limiter = rateLimit({
  windowMs: process.env.RATE_LIMIT_WINDOW_MS, // 15 minutes (default)
  max: process.env.RATE_LIMIT_MAX, // limit each IP to 100 (default) requests per windowMs
});
app.use(limiter);
// Other middleware
app.use(cors());
app.use(express.json());

// Routes
app.use("/auth", authRoutes);
app.use("/api/patients", patientRoutes);
app.use("/api/appointments", appointmentRouts);
app.use("/api/companies", companyRoutes);


// Connect to MongoDB
mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.listen(5000, () => console.log('Server running on port 5000'));
