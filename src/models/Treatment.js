const mongoose = require('mongoose');

const TreatmentSchema = new mongoose.Schema(
  {
    patientId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'PatientRecord',
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    }, // Professional who created the treatment
    templateType: { type: String, required: true }, // BIRP or DAP
    behaviorOrData: { type: String, default: null }, // Behavior (BIRP) or Data (DAP)
    interventionOrAssessment: { type: String, default: null }, // Intervention (BIRP) or Assessment (DAP)
    response: { type: String, default: null }, // Response (BIRP only)
    plan: { type: String, required: true }, // Plan (common to both templates)
    startDate: { type: Date, required: true }, // Start date of the treatment
    endDate: { type: Date, default: null }, // End date of the treatment (if applicable)
    status: { type: String, default: 'active' }, // Status of the treatment (e.g., active, completed, canceled)
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Treatment', TreatmentSchema);
