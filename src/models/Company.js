const mongoose = require('mongoose');

const CompanySchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    vatNumber: { type: String, unique: true },
    vatVerificationDate: { type: Date, default: null },
    address: { type: String, default: null },
    phone: { type: String, default: null },
    email: { type: String, default: null },
    billingAddress: { type: String, default: null },
    subscriptionPlanId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'SubscriptionPlan',
      default: null,
    },
    subscriptionExpiry: { type: Date, default: null },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Company', CompanySchema);
