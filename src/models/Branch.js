const mongoose = require('mongoose');

const BranchSchema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Company',
      required: true,
    },
    name: { type: String, required: true },
    address: { type: String, default: null },
    phone: { type: String, default: null },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Branch', BranchSchema);
