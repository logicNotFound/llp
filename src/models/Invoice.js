const mongoose = require('mongoose');

const InvoiceSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    subscriptionPlanId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'SubscriptionPlan',
      required: true,
    },
    amount: { type: Number, required: true },
    currency: { type: String, required: true, maxlength: 3 },
    status: { type: String, required: true, maxlength: 20 },
    billingDate: { type: Date, required: true },
    dueDate: { type: Date, required: true },
    notes: { type: String, default: null },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Invoice', InvoiceSchema);
