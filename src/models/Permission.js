const mongoose = require('mongoose');

const PermissionSchema = new mongoose.Schema(
  {
    name: { type: String, unique: true, required: true },
    description: { type: String, default: null },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Permission', PermissionSchema);
