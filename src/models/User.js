const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  firstName: { type: String },
  lastName: { type: String },
  trialEndsAt: { type: Date, default: null },
  emailVerifiedAt: { type: Date, default: null },
  dateOfBirth: { type: Date, default: null },
  vatNumber: { type: String, unique: true, default: null },
  isFreelancer: { type: Boolean, default: null },
  subscribedFromTrial: { type: Boolean, default: false },
  profession: {
    type: String,
    enum: ['psychologist', 'counselor', 'coach', 'psychiatrist'],
    default: null,
  },
  accountType: {
    type: String,
    enum: ['main', 'secretary', 'professional'],
    default: 'main',
  },
  address: { type: String, default: null },
  phone: { type: String, default: null },
  language: { type: String, default: null },
  defaultTemplate: { type: String, enum: ['BIRP', 'DAP'], default: 'BIRP' },
  status: { type: String, default: 'active' },
  companyId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Company',
    default: null,
  },
  branchId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Branch',
    default: null,
  },
  roleId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role',
    default: null,
  },
  subscriptionPlanId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'SubscriptionPlan',
    default: null,
  },
});

UserSchema.pre('save', async function (next) {
  if (this.isModified('password')) {
    this.password = await bcrypt.hash(this.password, 12);
  }
  next();
});

UserSchema.methods.comparePassword = function (candidatePassword) {
  return bcrypt.compare(candidatePassword, this.password);
};

module.exports = mongoose.model('User', UserSchema);
