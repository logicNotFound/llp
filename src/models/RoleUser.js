const mongoose = require('mongoose');

const RoleUserSchema = new mongoose.Schema(
  {
    roleId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Role',
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

RoleUserSchema.index({ roleId: 1, userId: 1 }, { unique: true });

module.exports = mongoose.model('RoleUser', RoleUserSchema);
