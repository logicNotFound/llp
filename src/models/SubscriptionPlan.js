const mongoose = require('mongoose');

const SubscriptionPlanSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    description: { type: String, default: null },
    price: { type: Number, required: true },
    currency: { type: String, required: true, maxlength: 3 },
    billingCycle: {
      type: String,
      enum: ['monthly', 'quarterly', 'yearly'],
      required: true,
    },
    trialDays: { type: Number, default: 0 },
    isForCompany: { type: Boolean, default: false },
    pricePerUser: { type: Number, default: null },
    allowsAdditionalProfessionalAccounts: { type: Boolean, default: false },

    // Stripe-specific fields
    stripePlanId: { type: String, required: true },
    stripeProductId: { type: String, required: true },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('SubscriptionPlan', SubscriptionPlanSchema);
