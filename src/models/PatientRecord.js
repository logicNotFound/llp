const mongoose = require('mongoose');

const PatientRecordSchema = new mongoose.Schema(
  {
    branchId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Branch',
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    patientFirstName: { type: String, required: true },
    patientLastName: { type: String, required: true },
    dateOfBirth: { type: Date, default: null },
    address: { type: String, default: null },
    phone: { type: String, default: null },
    email: { type: String, default: null },
    notes: { type: String, default: null },
    medicalHistory: { type: String, default: null },
    treatmentPlan: { type: String, default: null },
    nextAppointment: { type: Date, default: null },
    emergencyContact: { type: String, default: null },
    gender: { type: String, default: null },
    insuranceInformation: { type: String, default: null },
    status: { type: String, default: 'active' },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('PatientRecord', PatientRecordSchema);
