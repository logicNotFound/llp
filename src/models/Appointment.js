const mongoose = require('mongoose');

const AppointmentSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    patientId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'PatientRecord',
      required: true,
    },
    scheduledDate: { type: Date, required: true },
    status: { type: String, required: true },
    notes: { type: String, default: null },
    location: { type: String, default: null },
    appointmentType: { type: String, default: null },
    recurringInterval: { type: String, default: null },
    recurringEndDate: { type: Date, default: null },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Appointment', AppointmentSchema);
