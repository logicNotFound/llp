const jwt = require('jsonwebtoken');
const User = require('../models/User');

exports.protect = async (req, res, next) => {
  let token;

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  }

  // Check if token is invalidated
  if (invalidatedTokens.includes(token)) {
    // Should not be used on production, we can use something like Redis for that.
    return res.status(401).json({ error: 'Token has been invalidated' });
  }

  if (!token) {
    return res
      .status(401)
      .json({ error: 'Not authorized to access this resource' });
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findById(decoded.id);

    if (!user) {
      return res.status(401).json({ error: 'No user found with this ID' });
    }

    req.user = user;
    next();
  } catch (err) {
    return res
      .status(401)
      .json({ error: 'Not authorized to access this resource' });
  }
};
