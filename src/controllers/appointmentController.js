const Appointment = require("../models/Appointment");

exports.createAppointment = async (req, res) => {
  try {
    const appointment = new Appointment({ ...req.body, userId: req.user._id });
    await appointment.save();
    res.status(201).json(appointment);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

exports.getAppointments = async (req, res) => {
  try {
    const appointments = await Appointment.find({ userId: req.user._id });
    res.status(201).json(appointments);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

exports.updateAppointment = async (req, res) => {
  try {
    const appointment = await Appointment.findOneAndUpdate(
      { _id: req.params.id, userId: req.user._id },
      req.body,
      { new: true, runValidators: true }
    );

    if (!appointment) {
      res.status(404).json({ error: "Appointment not found" });
    }
    res.status(201).json(appointment);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};


exports.deleteAppointment = async (req, res) => {
    try {
      const appointment = await appointment.findOneAndDelete({
        _id: req.params.id,
        userId: req.user._id,
      });
  
      if (!appointment) {
        return res.status(404).json({ error: "appointment not found" });
      }
  
      res.status(200).json({ message: "appointment deleted" });
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  };
