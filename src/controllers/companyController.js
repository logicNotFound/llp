const Company = require("../models/Company");

exports.createCompany = async (req, res) => {
  try {
    const company = new Company(req.body);
    await company.save();
    res.status(201).json(company);
  } catch (error) {
    res.status(404).json({ error: error.message });
  }
};
exports.getCompany = async (req, res) => {
  try {
    const company = await Company.find({ vatNumber: req.vat });
    res.status(200).json(company);
  } catch (error) {
    res.status(404).json({ error: error.message });
  }
};

exports.updateCompany = async (req, res) => {
  try {
    const company = await Company.findByIdAndUpdate(
      {
        vatNumber: req.vat,
      },
      req.body,
      { new: true, runValidators: true }
    );
    if (!company) {
      return res.status(404).json({ error: "Company not found" });
    }
    res.status(200).json(company);
  } catch (error) {
    res.status(404).json({ error: error.message });
  }
};

exports.deleteCompany = async (req, res) => {
  try {
    const company = await Company.findOneAndDelete({
      vatNumber: req.vat,
    });

    if (!company) {
      return res.status(404).json({ error: "company not found" });
    }

    res.status(200).json({ message: "company deleted" });
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};
