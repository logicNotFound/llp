const express = require('express');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const axios = require('axios');
const router = express.Router();
const { protect } = require('../middleware/auth');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const mongoose = require('mongoose');
const User = require('../models/User');
const Company = require('../models/Company');
const Role = require('../models/Role');
const SubscriptionPlan = require('../models/SubscriptionPlan');

// This is not suitable for production, In a production environment,we need a database like Redis for this
const invalidatedTokens = [];

// Register Route
router.post(
  '/register',
  [
    check('email').isEmail().withMessage('Invalid email format'),
    check('password')
      .isLength({ min: 8 })
      .withMessage('Password must be at least 8 characters'),
    check('vatNumber')
      .isLength({ min: 9 })
      .isAlphanumeric()
      .withMessage('Invalid VAT number'),
    check('firstName').notEmpty().withMessage('First name is required'),
    check('lastName').notEmpty().withMessage('Last name is required'),
    check('profession')
      .isIn(['psychologist', 'counselor', 'coach', 'psychiatrist'])
      .withMessage('Invalid profession'),
    check('roleIdentity')
      .isIn(['freelancer', 'company'])
      .withMessage('Invalid role identity'),
    check('language')
      .isLength({ min: 2 })
      .withMessage('Language must be at least 2 characters'),
    check('acceptTerms')
      .isBoolean()
      .equals('true')
      .withMessage('Terms must be accepted'),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const vatResult = await verifyVATNumber(
        req.body.vatNumber.substring(0, 2),
        req.body.vatNumber.substring(2),
      );
      if (!vatResult.valid) {
        return res.status(400).json({ message: 'Invalid VAT number' });
      }

      const company = new Company({
        name: vatResult.name || req.body.companyName,
        vat_number: req.body.vatNumber,
        address: vatResult.address,
      });
      await company.save();

      const user = new User({
        email: req.body.email,
        password: req.body.password,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        vatNumber: req.body.vatNumber,
        isFreelancer: req.body.roleIdentity === 'freelancer',
        profession: req.body.profession,
        language: req.body.language,
        companyId: company._id,
        trialEndsAt: new Date(Date.now() + 10 * 24 * 60 * 60 * 1000), // 10 days from now
        acceptTerms: req.body.acceptTerms,
        address: vatResult.address || null,
        phone: req.body.phone || null,
        accountType: 'main', // Default value
        status: 'active', // Default value
        defaultTemplate: 'BIRP', // Default value
      });

      await user.save();

      const role = await Role.findOne({ name: 'trial_user' });
      user.roleId = role._id;
      await user.save();

      const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
        expiresIn: '1d',
      });

      res.status(201).json({ token, user });
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  },
);

// Login route
router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });

    if (!user || !(await user.comparePassword(password))) {
      return res.status(401).json({ error: 'Invalid email or password' });
    }

    const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
      expiresIn: '1d',
    });
    res.json({ token, user });
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
});

// Logout route
router.post('/logout', protect, async (req, res) => {
  try {
    // Invalidate the token here (depends on your token storage strategy)
    // You can add the token to a blacklist, or change a flag in the database
    // For example, you could set a 'tokenInvalid' field in the User model
    const token = req.headers.authorization.split(' ')[1];
    invalidatedTokens.push(token);
    res.status(200).json({ message: 'Logged out successfully' });
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
});

// User Profile route
router.get('/user-profile', async (req, res) => {
  try {
    // Extract user ID from the token (assuming the token is in the header)
    const token = req.header('Authorization').replace('Bearer ', '');
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded.id;

    // Find the user by ID
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    // Return user profile
    res.status(200).json({ user });
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
});

// Update User Profile route
router.put(
  '/update-profile',
  [
    protect,
    check('first_name')
      .optional()
      .isString()
      .withMessage('First name must be a string')
      .trim()
      .escape(),
    check('last_name')
      .optional()
      .isString()
      .withMessage('Last name must be a string')
      .trim()
      .escape(),
    check('email')
      .optional()
      .isEmail()
      .withMessage('Invalid email format')
      .normalizeEmail(),
    check('password')
      .optional()
      .isLength({ min: 8 })
      .withMessage('Password must be at least 8 characters'),
    check('default_template')
      .optional()
      .isIn(['BIRP', 'DAP'])
      .withMessage('Invalid template type'),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      // Extract user ID from the token
      const userId = req.user.id;

      // Find the user by ID
      const user = await User.findById(userId);
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }

      // Update user fields
      user.firstName = req.body.first_name || user.firstName;
      user.lastName = req.body.last_name || user.lastName;
      user.email = req.body.email || user.email;
      user.defaultTemplate = req.body.default_template || user.defaultTemplate;

      // Update password if provided
      if (req.body.password) {
        user.password = await bcrypt.hash(req.body.password, 10);
      }

      // Save the updated user
      await user.save();

      res.status(200).json({ message: 'Profile updated successfully', user });
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  },
);

// Delete User Account
router.delete('/delete-account', protect, async (req, res) => {
  try {
    // Extract user ID from the token
    const userId = req.user.id;

    // Find the user by ID
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    // Soft delete the user by setting a 'deletedAt' field
    user.deletedAt = new Date();
    await user.save();

    // Invalidate the token
    const token = req.headers.authorization.split(' ')[1];
    invalidatedTokens.push(token);

    res.status(200).json({ message: 'Account deleted successfully' });
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
});

// Restore User Account
router.patch('/restore/:deletedUserId', protect, async (req, res) => {
  try {
    // Check if the current user is an admin
    const currentUser = await User.findById(req.user.id);
    if (!currentUser.hasRole('admin')) {
      return res.status(403).json({ message: 'Unauthorized' });
    }

    // Find the user including soft-deleted users
    const deletedUserId = mongoose.Types.ObjectId(req.params.deletedUserId);
    const deletedUser = await User.findOne({
      _id: deletedUserId,
      deletedAt: { $exists: true },
    });

    if (!deletedUser) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Restore the user by removing the 'deletedAt' field
    deletedUser.deletedAt = undefined;
    await deletedUser.save();

    // Optionally, remove the token from the invalidatedTokens list
    const index = invalidatedTokens.indexOf(deletedUser.token);
    if (index > -1) {
      invalidatedTokens.splice(index, 1);
    }

    res.status(200).json({ message: 'User restored successfully' });
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
});

// Request Password Reset route
router.post(
  '/request-password-reset',
  [check('email').isEmail().withMessage('Invalid email format')],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const { email } = req.body;
      const user = await User.findOne({ email });

      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      // Generate a token for the password reset
      const token = crypto.randomBytes(20).toString('hex');

      // Store the token in the user model
      user.resetPasswordToken = token;
      user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
      await user.save();

      // Send the password reset email
      const transporter = nodemailer.createTransport({
        host: process.env.SMTP_SERVER,
        port: process.env.SMTP_PORT,
        auth: {
          user: process.env.SMTP_USER,
          pass: process.env.SMTP_PASSWORD,
        },
      });

      const mailOptions = {
        to: user.email,
        from: process.env.EMAIL_ADDRESS,
        subject: 'Password Reset',
        text: `You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\nPlease click on the following link, or paste this into your browser to complete the process within one hour of receiving it:\n\nhttp://${req.headers.host}/reset/${token}\n\nIf you did not request this, please ignore this email and your password will remain unchanged.\n`,
      };

      transporter.sendMail(mailOptions, (err) => {
        if (err) {
          return res.status(500).json({ message: 'Email could not be sent' });
        }
        res.status(200).json({ message: 'Password reset email sent.' });
      });
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  },
);

// Reset Password
router.post('/reset-password', async (req, res) => {
  try {
    const { token, email, password } = req.body;

    // Validate the request data
    if (!token || !email || !password) {
      return res.status(400).json({ message: 'All fields are required.' });
    }

    // Check if the token is valid
    const tokenData = await PasswordResetToken.findOne({ token, email });
    if (!tokenData) {
      return res.status(400).json({ message: 'Invalid token or email.' });
    }

    // Check if the token has expired
    const isExpired = Date.now() > new Date(tokenData.expiresAt).getTime();
    if (isExpired) {
      return res.status(400).json({ message: 'Token has expired.' });
    }

    // Update the user's password
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    user.password = hashedPassword;
    await user.save();

    // Delete the token
    await PasswordResetToken.deleteOne({ email });

    res.status(200).json({ message: 'Password reset successful.' });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Change Subscription
router.post('/change-subscription', protect, async (req, res) => {
  try {
    const { subscription_plan, additional_secretaries } = req.body;
    const userId = req.user.id;

    // Validate the request data
    if (!subscription_plan) {
      return res
        .status(400)
        .json({ message: 'Subscription plan is required.' });
    }

    // Fetch the subscription plan the user wants to change to
    const newSubscriptionPlan = await SubscriptionPlan.findOne({
      name: subscription_plan,
    });
    if (!newSubscriptionPlan) {
      return res.status(400).json({ message: 'Invalid subscription plan.' });
    }

    // Find the user
    const user = await User.findById(userId);
    if (user.roleId.toString() === Role.TrialUser) {
      user.subscribed_from_trial = true;
      user.trial_ends_at = null; // End the trial
    }

    // Update the user's subscription plan
    user.subscription_plan_id = newSubscriptionPlan._id;

    // Detach all subscription roles from the user
    await user.updateOne({
      $pull: {
        roles: { $in: [Role.TrialUser, Role.BasicPlan, Role.PremiumPlan] },
      },
    });

    // Attach the new subscription role to the user
    const subscriptionRole = await Role.findOne({
      name: newSubscriptionPlan.getSubscriptionRoleForPlan(),
    });
    user.roles.push(subscriptionRole._id);

    await user.save();

    res.status(200).json({ message: 'Successfully changed subscription!' });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// const { check, validationResult } = require("express-validator");
// const Role = require("../models/Role");
// const User = require("../models/User");

// Add Secretary
router.post(
  '/add-secretary/:id',
  [
    check('firstName').notEmpty().withMessage('First name is required'),
    check('lastName').notEmpty().withMessage('Last name is required'),
    check('email').isEmail().withMessage('Invalid email format'),
    check('password')
      .isLength({ min: 8 })
      .withMessage('Password must be at least 8 characters')
      .matches(/\d/)
      .withMessage('Password must contain a number'),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const userId = req.params.id;
      const user = await User.findById(userId);

      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      // Check if the user is a professional
      if (!user.isProfessional()) {
        return res.status(403).json({ message: 'Unauthorized' });
      }

      // Check if the professional already has a secretary
      const secretaryCount = await User.countDocuments({
        companyId: user.companyId,
        profession: 'secretary',
      });

      if (secretaryCount >= 1) {
        return res.status(400).json({
          message:
            'You have reached your maximum number of secretary accounts.',
        });
      }

      const secretary = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        accountType: 'secretary',
        companyId: user.companyId,
      });

      await secretary.save();

      // Assign the secretary role to the new user
      const role = await Role.findOne({ name: 'secretary' });
      secretary.roleId = role._id;
      await secretary.save();

      res.status(201).json({ message: 'Successfully added secretary!' });
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  },
);

// Change Subscription
router.post(
  '/change-subscription',
  [
    check('subscription_plan')
      .notEmpty()
      .withMessage('Subscription plan is required'),
    check('additional_secretaries')
      .optional()
      .isInt({ min: 0 })
      .withMessage('Additional secretaries must be a non-negative integer'),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const userId = req.user.id; // Assuming you have middleware that sets req.user
      const user = await User.findById(userId);

      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      const newSubscriptionPlan = await SubscriptionPlan.findOne({
        name: req.body.subscription_plan,
      });

      if (!newSubscriptionPlan) {
        return res.status(400).json({ message: 'Invalid subscription plan' });
      }

      // If the user is currently a trial user, end the trial
      if (user.hasRole('trial_user')) {
        user.subscribedFromTrial = true;
        user.trialEndsAt = null;
      }

      // Update the user's subscription plan
      user.subscriptionPlanId = newSubscriptionPlan._id;

      // Detach all subscription roles and attach the new one
      const allSubscriptionRoles = await Role.find({
        name: { $in: ['trial_user', 'basicPlanRole', 'premiumPlanRole'] },
      });

      user.roleId = null; // Remove existing role
      await user.save();

      const newRole = await Role.findOne({
        name: newSubscriptionPlan.getSubscriptionRoleForPlan(),
      });
      user.roleId = newRole._id;
      await user.save();

      res.status(200).json({ message: 'Successfully changed subscription!' });
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  },
);

// Vat Verification using VIES API
async function verifyVATNumber(countryCode, vatNumber) {
  try {
    const response = await axios.post(process.env.VIES_API_URL, {
      countryCode,
      vatNumber,
    });
    return {
      valid: true,
      name: response.data.name,
      address: response.data.address,
    };
  } catch (error) {
    return { valid: false };
  }
}

module.exports = router;
